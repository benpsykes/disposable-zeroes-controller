# disposable-zeroes-controller

An arduino based controller for firing a disposable camera shutter with a solenoid.


Code is pretty basic, it assumes a solenoid is powered via a MOSFET switching circuit on pin 9, and pin 10 has a status LED.

Controlled via an IR remote - if a different IR remote is used then the IR remote hex codes will need to be placed in the main run loop.
