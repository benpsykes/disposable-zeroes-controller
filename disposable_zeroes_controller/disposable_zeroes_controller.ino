#include <IRremote.h>

const int RECV_PIN = 7;        // IR Receiver signal pin.
const int LED_PIN = 9;         // LED status light output pin.
const int SOLENOID_PIN = 10;   // Solenoid/MOSFET driver output pin.
const int SOLENOID_TIME = 2000; //how long to fire the solenoid for - 2 seconds normally.
IRrecv irrecv(RECV_PIN);
decode_results results;

void setup(){
  //Setup IR receiving
  Serial.begin(9600);
  irrecv.enableIRIn();
  //irrecv.blink13(true);
  //Setup output pins
  pinMode(LED_PIN,OUTPUT);       //LED pin is an output
  pinMode(SOLENOID_PIN,OUTPUT);  //Solenoid pin is an output
  
}

void loop(){
  if (irrecv.decode(&results)){
        Serial.println(results.value, HEX); // write out IR code to serial port for debugging / code finding
        switch(results.value){
          case 0xFF6897: //keypad 1 on our generic IR remote
              timedShutter(1000);
              break;
           case 0xFF9867: //keypad 2
              timedShutter(2000);
              break;
           case 0xFFB04F: //3:
              timedShutter(3000);
              break;
           
           //**todo: handle  the rest of the number buttons
           
           case 0xFF02FD: //OK
              instantShutter();
              break;
           case 0xFF42BD: //*

           break;
        }
        irrecv.resume();
  }
}
//delay a shutter release by "delay" msec then fire the solenoid for 2 seconds.
void timedShutter(int _delay)
{
      int firstDelay = _delay / 4;            //calculate out the delays between switching the LEDs - it gets faster until the shutter press.
      
      int secondDelay = _delay / 8;
      int thirdDelay = _delay / 16;
      int fourthDelay = _delay / 32;
      _lightLED(firstDelay,firstDelay,1);
      _lightLED(secondDelay,secondDelay,2);
      _lightLED(thirdDelay,thirdDelay,4);
      _lightLED(fourthDelay,fourthDelay,8);
      delay(100);
      digitalWrite(SOLENOID_PIN,HIGH);          //fire the solenoid for 2 seconds
      delay(SOLENOID_TIME);
      digitalWrite(SOLENOID_PIN,LOW);
      _lightLED(1000,0,1);
}
void _lightLED(int time,int _delay,int loops)
{
  for (int i = 0; i < loops; i++)
  {
    digitalWrite(LED_PIN,HIGH);
    delay(time);
    digitalWrite(LED_PIN,LOW);
    delay(_delay);
  }
}

void instantShutter()
{
    //fire both LED and solenoid for 2 seconds then reset
    digitalWrite(LED_PIN,HIGH);
    digitalWrite(SOLENOID_PIN,HIGH);
    delay(SOLENOID_TIME);
    digitalWrite(LED_PIN,LOW);
    digitalWrite(SOLENOID_PIN,LOW);
}

